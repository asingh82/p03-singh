//
//  GameView.m
//  DoodleJump
//
//  Created by Anshima on 19/02/17.
//  Copyright © 2017 Q Corp. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize jumper, bricks;
@synthesize tilt;
@synthesize score;

int globalCounter = -1;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        CGRect bounds = [self bounds];
        
        jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 20, 20)];
        // [jumper setBackgroundColor:[UIColor redColor] isAlpha:1];
        UIImageView * olaf = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        olaf.image = [UIImage imageNamed:@"olaf1.png"];
        olaf.alpha = 1;
        [jumper setDx:0];
        [jumper setDy:10];
        [jumper addSubview:olaf];
        [self addSubview:jumper];
        [self makeBricks:nil];
    }
    return self;
}

-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    float width = bounds.size.width * .2;
    float height = 20;
    
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    
    bricks = [[NSMutableArray alloc] init];
    for (int i = 0; i < 15; ++i)
    {
        Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
      //  [b setBackgroundColor:[UIColor lightGrayColor]];
        UIImageView * brick = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        brick.image = [UIImage imageNamed:@"bricks.png"];
        brick.alpha = 1;
        [b addSubview:brick];
        [self addSubview:b];
        [b setCenter:CGPointMake(rand() % (int)(bounds.size.width), rand() % (int)(bounds.size.height))];
        [bricks addObject:b];
    }
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(void)arrange:(CADisplayLink *)sender
{
    // CFTimeInterval ts = [sender timestamp];
    // int i=0;
    CGRect bounds = [self bounds];
    
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    [jumper setDx:[jumper dx] + tilt];
    if ([jumper dx] > 5)
        [jumper setDx:5];
    if ([jumper dx] < -5)
        [jumper setDx:-5];
    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    p.y -= [jumper dy];
    
    // If the jumper has fallen below the bottom of the screen,
    // add a positive velocity to move him
    if (p.y > bounds.size.height)
    {
        [jumper setDy:20];
        p.y = bounds.size.height;
        [self gameOver];
    }
    
    // If we've gone past the top of the screen, wrap around
    if (p.y < 0)
    {
        globalCounter++;
        p.y += bounds.size.height;
        [self makeBricks:nil];
      //  NSLog(@"i ki value: %d",globalCounter);
        if(globalCounter%3 == 0){
            NSLog(@"i for 0: %d",globalCounter);
   /*         UIImage *bgImage = [UIImage imageNamed:@"bg1.jpg"];
            UIImageView *backgroundImageView1 =[[UIImageView alloc]initWithFrame:self.superview.frame];
            backgroundImageView1.image = bgImage;
            backgroundImageView1.alpha = 1;
            [self.superview insertSubview:backgroundImageView1 atIndex:0];
    */    }
        else if(globalCounter%3 == 1){
          NSLog(@"i for 1: %d",globalCounter);
   /*           UIImage *bgImage1 = [UIImage imageNamed:@"bg2.jpg"];
            UIImageView *backgroundImageView2 =[[UIImageView alloc]initWithFrame:self.superview.frame];
            backgroundImageView2.image = bgImage1;
            [self.superview insertSubview:backgroundImageView2 atIndex:0];
  */     }
        else{
            NSLog(@"i for 2: %d",globalCounter);
 /*           UIImage *bgImage2 = [UIImage imageNamed:@"bg3.jpg"];
            UIImageView *backgroundImageView3 =[[UIImageView alloc]initWithFrame:self.superview.frame];
            backgroundImageView3.image = bgImage2;
            [self.superview insertSubview:backgroundImageView3 atIndex:0];
  */        globalCounter=-1;
        }
    }
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0)
        p.x += bounds.size.width;
    if (p.x > bounds.size.width)
        p.x -= bounds.size.width;
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.?
    if ([jumper dy] < 0)
    {
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, p))
            {
                // Yay!  Bounce!
                //  NSLog(@"Bounce!");
                [jumper setDy:10];
            }
        }
    }
    
    [jumper setCenter:p];
    // NSLog(@"Timestamp %f", ts);
}

-(void)gameOver{
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    [jumper removeFromSuperview];
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurView = [[UIVisualEffectView alloc] initWithEffect:blur];
    blurView.frame = [self bounds];
    blurView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:blurView];
    
    UILabel *gameOverLabel = [[UILabel alloc] init];
    [gameOverLabel setText:@"Game Over"];
    [gameOverLabel setTextColor:[UIColor whiteColor]] ;
    [gameOverLabel sizeToFit];
    [gameOverLabel setCenter: self.center];
    [[blurView contentView] addSubview: gameOverLabel];
    [self hideSlider];
}

-(void)hideSlider{
    UISlider *slider1 = [self.superview viewWithTag:1];
    if (slider1) {
        NSLog(@"Value of slider 1: %f", slider1.value);
    }
    slider1.alpha = 0;
}
@end
